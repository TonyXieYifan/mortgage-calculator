package edu.sjsu.android.project1tonyxie;

public class CalcMortage {
    public static double calcMortage(float interest, float principal, int year, boolean selected) {
        float numMonth = 12 * year;
        float i = interest / 1200;
        if(interest == 0){
            if(selected){
                return ((principal/numMonth) + (0.001 * principal));
            }else{
                return principal/numMonth;
            }
        }else{
            if(selected){
                return (((principal*i)/(1 - Math.pow((1+i),(-numMonth)))) + (0.001 * principal));
            }else{
                return (principal*i)/(1 - Math.pow((1+i),(-numMonth)));
            }
        }
    }
}
