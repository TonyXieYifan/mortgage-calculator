package edu.sjsu.android.project1tonyxie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity {

    private EditText input;
    private TextView percent;
    private Float show = 10f;
    private String str;
    private SeekBar bar;
    private RadioButton rb1;
    private RadioButton rb2;
    private RadioButton rb3;
    private CheckBox box;
    private TextView result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        input = findViewById(R.id.inputText);
        percent = findViewById(R.id.percent);
        bar = findViewById(R.id.seekBar);
        rb1 = findViewById(R.id.radioButton);
        rb2 = findViewById(R.id.radioButton2);
        rb3 = findViewById(R.id.radioButton3);
        box = findViewById(R.id.checkBox);
        result = findViewById(R.id.result);

        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                show = (float)(progress * 0.1);
                str = Float.toString(show);
                percent.setText(str + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void onClick(View view) {
        // Toast a message for empty entry
        if (input.getText().length() == 0) {
            Toast.makeText(this, "Invalid Input!",
                    Toast.LENGTH_LONG).show();
            result.setText("Please enter the principle.\nThen press CALCULATE for monthly payments.");
            return;
        }

        String str1 = input.getText().toString();
        if(str1.contains(".") && str1.substring(str1.indexOf(".")).length() > 3){
            Toast.makeText(this, "Invalid Input!",
                    Toast.LENGTH_LONG).show();
            result.setText("Please enter a valid number. 2 decimal digits max.\nThen press CALCULATE for monthly payments.");
            return;
        }


        // Get the input value from the input box
        float inputValue = Float.parseFloat(input.getText().toString());
        float interest = show;
        boolean checked = box.isChecked();
        if (rb1.isChecked()) {
            // Set the text box to be the result of mortage
            result.setText("Monthly payment: $" + String.format("%.2f", CalcMortage.calcMortage(interest,
                    inputValue,15, checked)));
        } else if(rb2.isChecked()){
            result.setText("Monthly payment: $" + String.format("%.2f", CalcMortage.calcMortage(interest,
                    inputValue,20, checked)));
        }else{
            result.setText("Monthly payment: $" + String.format("%.2f", CalcMortage.calcMortage(interest,
                    inputValue,30, checked)));
        }
    }

        public void offClick (View view){
            Intent delete = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
            startActivity(delete);
        }
    }
